# oVirt Manager Bot

Телеграм бот для инветаризации (поиск по имени, MAC-адресу, IP-адресу и т.п) виртуальных машин в oVirt Manager.
___
## Что нужно?
- [Python](https://www.python.org/)     >= 3.11
- [Docker](https://www.docker.com/) (если запускаете в докере)

## Настройка

Переименовать **config-example.json** в **config.json**

В config.json:
```
{
  "bot_token": "<TELEGRAM BOT TOKEN>",
  "bot_access": [],
  "bot_ipa_username": "<IPA BOT USERNAME>",
  "bot_ipa_password": "<IPA BOT PASSWORD>",
  "sync_period": 86400,
  "clean_period": 31536000,
  "sync_timeout": 1800,
  "sync_on_startup": true,
  "db_url": "sqlite:////var/lib/ovirt-manager-bot/database.db",
  "log_file": "bot.log",
  "engines": [
    "https://<ENGINE1 LINK>",
    "https://<ENGINE2 LINK>",
    "..."
  ]
}
```

- **bot_token** - Токен Telegram-бота
- **bot_access** - Список chat_id (для всей группы) или user_id (для определенного пользователя) которым можно использовать бота. Узнать идентификаторы можно командой /start при первом запуске.
- **bot_ipa_username** - Имя пользовтеля от FREEIPA
- **bot_ipa_password** - Пароль от FREEIPA
- **sync_period** - Период (в секундах) обновления данных о виртуализациях
- **clean_period** - Период (в секундах) очистки информации об удаленных ВМ из базы
- **sync_timeout** - Таймаут (в секундах) перед обновлениями. Не должно быть больше чем sync_period
- **sync_on_startup** - Запустить сканирование при запуске
- **db_url** - Директория где хранится база данных sqlite
- **log_file** - Путь до лог файла
- **engines** - Список url адрессов для каждого OvirtEngin'a. Например: https://ovirtmanager.local/.


## Запуск
Не забыть создать директорию для базы данных, по умолчанию в ```/var/lib/ovirt-manager-bot/```

#### Запуск локально
``` python3 -m venv .venv && source ./venv/bin/activate ```

``` python3 -m pip install -r requiremets.txt ```

``` python3 -m app -c config.json ```

#### Запуск в докере
``` docker compose -f ovirt-bot-compose.yml up```

## Команды
```
/status - статистика по всем виртуализациям
/vm_last <count:default 10> - последние count созданных ВМ
/vm_lost <count:default 10> - забытые ВМ которые выключились и не включались
/vm_name <имя> - поиск ВМ по имени
/vm_mac <mac> - поиск ВМ по MAC-адресу
/vm_mac <имя> - MAC-адресса виртуальной машины
/vm_ip <ip> - поиск ВМ по IP-адресу
/vm_ip <имя> - IP-адреса виртуальной машины
/vm_sync_diff <дата> - разница текущим состоянием машин и в указанную дату
/find_export <Storage Domain> - поиск виртуализации к которой подключено хранилище
/vm_scan <network or ip-range> - поиск виртуальных машин в указанной сети или диапазоне адресов
```
