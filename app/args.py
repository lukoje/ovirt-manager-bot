from argparse import ArgumentParser


def setup_arguments():
    parser = ArgumentParser()

    parser.add_argument(
        "-c", "--config", help="Config file", default="config.json", required=True
    )
    parser.add_argument("-L", "--logfile", help="Logging file", required=False)
    parser.add_argument("-d", "--db-file", help="SQLite db file.")
    parser.add_argument(
        "--verbose",
        action="store_true",
        default=False,
        help="Enable verbosity",
        required=False,
    )
    return parser.parse_args()
