from sqlite3 import Connection as SQLite3Connection

from sqlalchemy import create_engine, event
from sqlalchemy.engine import Engine
from sqlalchemy.orm import DeclarativeBase, sessionmaker
from contextlib import contextmanager


class Base(DeclarativeBase): ...


class Database:
    def __init__(self):
        self.engine = None
        self.local_session = None

    def init(self, config):
        self.engine = create_engine(config.get("db_url"))
        self.local_session = sessionmaker(bind=self.engine)

        if self.engine.driver == "pysqlite":

            @event.listens_for(Engine, "connect")
            def _set_sqlite_pragma(dbapi_connection, connection_record):
                """ForeignKey support for sqlite3"""
                if isinstance(dbapi_connection, SQLite3Connection):
                    cursor = dbapi_connection.cursor()
                    cursor.execute("PRAGMA foreign_keys=ON;")
                    cursor.close()

        Base.metadata.create_all(self.engine)

    @contextmanager
    def get_session(self):
        if not self.local_session:
            raise RuntimeError("Database is not initialized. Call init(config) first.")

        session = self.local_session()
        try:
            yield session
        finally:
            session.close()


db = Database()
