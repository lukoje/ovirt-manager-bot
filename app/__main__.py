import logging
import sys
import traceback

from app.main import main

if __name__ == "__main__":
    try:
        sys.exit(main())
    except RuntimeError:
        logging.error(traceback.format_exc())
        exit(1)
