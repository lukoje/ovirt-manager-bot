from typing import List, Tuple, Optional

from sqlalchemy import desc, func, String
from sqlalchemy.orm import Session

from app.models import Engine, VM, Interface, IPAddress, SyncHistory
from datetime import datetime
import logging


def get_engines(session: Session) -> List[Engine]:
    return session.query(Engine).all()


def get_engine_by_name(session: Session, name: str) -> Engine:
    return session.query(Engine).filter(Engine.name == name).one_or_none()


def get_engine_by_url(session: Session, url: str) -> Engine:
    return session.query(Engine).filter(Engine.url == url).one_or_none()


def get_vm_by_hash(session: Session, vm_hash: str) -> VM:
    return session.query(VM).filter(VM.vm_hash == vm_hash).one_or_none()


def get_vms_by_name(session: Session, vm_name: str) -> List[VM]:
    return (
        session.query(VM)
        .filter(VM.vm_name.like(vm_name))
        .order_by(VM.deleted_at.asc())
        .all()
    )


def get_vms_by_engine(session: Session, engine: Engine) -> List[VM]:
    return session.query(VM).filter(VM.engine == engine).all()


def get_vms_by_mac_address(session: Session, mac_address: str) -> List[VM]:
    return (
        session.query(VM)
        .filter(VM.vm_ifaces.any(mac_address=mac_address))
        .order_by(VM.deleted_at.asc())
        .all()
    )


def get_vms_by_ip_address(session: Session, ip_address: str) -> List[VM]:
    return (
        session.query(VM)
        .join(VM.vm_ifaces)
        .join(Interface.ip_address)
        .filter(IPAddress.ip_address == ip_address)
        .order_by(VM.deleted_at.asc())
        .all()
    )


def get_vms_by_ip_addresses(session: Session, ip_addresses: list[str]) -> List[VM]:
    return (
        session.query(VM)
        .join(VM.vm_ifaces)
        .join(Interface.ip_address)
        .filter(IPAddress.ip_address.in_(ip_addresses))
        .order_by(VM.deleted_at.asc())
        .all()
    )


def get_last_created_vms(session: Session, count: int) -> Optional[List[VM]]:
    return (
        session.query(VM)
        .order_by(desc(VM.created_at))
        .order_by(VM.deleted_at.asc())
        .limit(count)
        .all()
    )


def get_lost_vms(session: Session, count: int = 0):
    if count <= 0:
        return (
            session.query(VM)
            .order_by(VM.stop_time)
            .filter(VM.start_time.is_(None))
            .all()
        )
    return (
        session.query(VM)
        .order_by(VM.stop_time)
        .filter(VM.start_time.is_(None))
        .order_by(VM.deleted_at.asc())
        .limit(count)
        .all()
    )


def add_or_update_vm(session: Session, vm: VM) -> None:
    vm_exists = get_vm_by_hash(session, vm.vm_hash)
    if not vm_exists:
        logging.debug(f"Add new VM: {vm.vm_name} ({vm.vm_hash})")
        session.add(vm)
        return

    logging.debug(f"VM {vm.vm_name} ({vm.vm_hash}) already exists!")
    session.query(VM).filter(VM.vm_hash == vm.vm_hash).update(
        {
            VM.vm_name: vm.vm_name,
            VM.engine_id: vm.engine_id,
            VM.vm_status: vm.vm_status,
            VM.start_time: vm.start_time,
            VM.stop_time: vm.stop_time,
        }
    )
    session.commit()
    return


def get_iface_by_hash(session: Session, iface_hash: str) -> Interface:
    return (
        session.query(Interface)
        .filter(Interface.iface_hash == iface_hash)
        .one_or_none()
    )


def add_or_update_interface(session: Session, iface: Interface) -> Optional[Interface]:
    iface_exists = get_iface_by_hash(session, iface.iface_hash)

    if not iface_exists:
        logging.debug(f"Add new Interface: {iface.iface_name} ({iface.iface_hash})")
        session.add(iface)
        return iface

    logging.debug(f"Interface {iface.iface_name} ({iface.iface_hash}) already exists!")
    session.query(Interface).filter(Interface.iface_hash == iface.iface_hash).update(
        {
            Interface.iface_name: iface.iface_name,
            Interface.mac_address: iface.mac_address,
        }
    )

    session.commit()
    return


def add_or_update_ip_address(
    session: Session, ipaddr: IPAddress
) -> Optional[Interface]:
    exist = (
        session.query(IPAddress)
        .filter(
            (IPAddress.ip_address == ipaddr.ip_address)
            & (IPAddress.iface_id == ipaddr.iface_id)
        )
        .one_or_none()
    )

    if exist is None:
        logging.debug(f"Add new IP address: {ipaddr.ip_address}")
        session.add(ipaddr)
        return ipaddr
    return


def clear_deleted_vms(session: Session) -> Optional[List[VM]]:
    vms = session.query(VM).filter(VM.deleted_at.is_not(None))
    deleted_vms = vms.all()
    vms.delete()
    session.commit()
    return deleted_vms


def write_sync_history(
    session: Session, sync_history: SyncHistory
) -> Optional[SyncHistory]:
    exist = (
        session.query(SyncHistory)
        .filter(SyncHistory.sync_datetime == sync_history.sync_datetime)
        .one_or_none()
    )

    if exist is None:
        logging.debug(f"Write sync data to sync history: {sync_history.sync_datetime}")
        session.add(sync_history)
        session.commit()
        return sync_history
    return


def show_sync_history(session: Session, count: int = 100) -> Optional[List[datetime]]:
    count = max(0, min(count, 100))
    sync_history = (
        session.query(SyncHistory)
        .order_by(SyncHistory.sync_datetime.desc())
        .limit(count)
        .all()
    )
    dates = [d.sync_datetime for d in sync_history]
    if dates:
        return dates
    else:
        return


def get_vms_diff_from_history(
    session: Session, sync_datetime: datetime
) -> Optional[List[Tuple[VM, VM]]]:
    sync_datetime = sync_datetime.replace(microsecond=0)

    vms = session.query(VM).all()
    sync_history = (
        session.query(SyncHistory)
        .filter(
            func.cast(SyncHistory.sync_datetime, String).like(
                f"{sync_datetime:%Y-%m-%d %H:%M:%S}%"
            )
        )
        .one_or_none()
    )

    if sync_history is None:
        return

    vms_from_history = sync_history.get_vms()

    vm_dict = {vm.vm_hash: vm for vm in vms}
    vm_dict_from_history = {vm.vm_hash: vm for vm in vms_from_history}

    vms_diff: List[Tuple[VM, VM]] = []

    vms_set = {vm.vm_hash: vm.vm_status for vm in vms}
    vms_from_history_set = {vm.vm_hash: vm.vm_status for vm in vms_from_history}

    diff_keys = vms_set.keys() & vms_from_history_set.keys()

    for vm_hash in diff_keys:
        v1_status = vms_set.get(vm_hash)
        v2_status = vms_from_history_set.get(vm_hash)
        if v1_status is not None and v2_status is not None and v1_status != v2_status:
            vms_diff.append((vm_dict[vm_hash], vm_dict_from_history[vm_hash]))

    return vms_diff
