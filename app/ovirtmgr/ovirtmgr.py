import concurrent.futures
import os
import threading
from datetime import datetime, timedelta
from typing import Optional
from urllib.parse import urlparse

import ovirtsdk4 as ovirt
from ovirtsdk4.types import IpVersion, StorageDomainStatus
from sqlalchemy import update

from app.models import Engine, VM, Interface, IPAddress, SyncHistory
from app.ovirtmgr import services
from app.ovirtmgr.utils import get_vm_hash, get_nic_hash
from app.scheduler import ThreadedScheduler, Timer
import logging
from app.config import REQUESTS_TIMEOUT
from app.database import db


class OvirtManager:
    def __init__(self, config: dict):
        self.engines = config.get("engines")
        self._config = config

        self._connections = []

        self._last_update: datetime = datetime.now()

        self.sync_thread = threading.Thread(target=self.sync)
        self.sync_thread.start()

        self.scheduler = ThreadedScheduler(
            [
                Timer(config.get("sync_period"), self.sync),
                Timer(config.get("clean_period"), self.clean_database),
            ]
        )
        self.scheduler.start()

    def update_engine_data(self, conn) -> None:

        engine_url = conn._engine_url
        engine_name = urlparse(engine_url).netloc
        available = conn.test()

        if not available:
            logging.error(f"No connection with {conn._engine_url}!")

        with db.get_session() as session:
            engine_exists = (
                session.query(Engine).filter(Engine.name == engine_name).one_or_none()
            )

            if engine_exists:
                logging.info(f"Engine {engine_name} exists. Update it")
                session.query(Engine).filter(Engine.name == engine_name).update(
                    {Engine.url: engine_url, Engine.available: available}
                )
                session.commit()
                return

            logging.debug(f"Add new engine: {engine_url}")
            eng = Engine(name=engine_name, url=engine_url, available=available)
            session.add(eng)
            session.commit()
        return

    def sync_engines(self) -> None:
        logging.info("Update engines data...")
        with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
            executor.map(self.update_engine_data, self._connections)

    def update_vms_data(self, conn) -> None:

        if conn is None:
            return

        vms_service = conn.system_service().vms_service()
        vms = vms_service.list()
        vms_count = len(vms)

        # VMs exists on engine
        actual_vms: list[VM] = []

        # Update data for this engine
        with db.get_session() as session:
            try:
                current_engine = services.get_engine_by_url(session, conn._engine_url)

                # Update vms count of engine
                session.execute(
                    update(Engine)
                    .where(Engine.url == conn._engine_url)
                    .values(vm_count=vms_count)
                )

                logging.info(f"Update vms data from {conn._engine_url}")
                for vm in vms:
                    vm_service = vms_service.vm_service(vm.id)
                    vm_created_at = datetime.fromisoformat(vm.creation_time.isoformat())
                    vm_hash = get_vm_hash(vm.id, vm_created_at.timestamp())

                    vm_status = vm.status.value
                    vm_start_time = (
                        datetime.fromisoformat(vm.start_time.isoformat())
                        if vm.start_time is not None
                        else None
                    )
                    vm_stop_time = (
                        datetime.fromisoformat(vm.stop_time.isoformat())
                        if vm.stop_time is not None
                        else None
                    )

                    vm_obj = VM(
                        vm_name=vm.name,
                        vm_hash=vm_hash,
                        engine_id=current_engine.id,
                        vm_status=vm_status,
                        created_at=vm_created_at,
                        start_time=vm_start_time,
                        stop_time=vm_stop_time,
                    )
                    # Add or update new VM
                    services.add_or_update_vm(session, vm_obj)
                    session.commit()

                    actual_vms.append(vm_obj)

                    # NICS Service
                    nics_service = vm_service.nics_service()
                    nics = nics_service.list()

                    for nic in nics:
                        _nic_hash = get_nic_hash(vm_hash, nic.id)
                        _vm_id = services.get_vm_by_hash(session, vm_hash).id

                        iface = Interface(
                            iface_hash=_nic_hash,
                            iface_name=nic.name,
                            vm_id=_vm_id,
                            mac_address=nic.mac.address,
                        )

                        services.add_or_update_interface(session, iface)

                        devices = (
                            vm_service.reported_devices_service().list()
                            or nic.reported_devices
                        )
                        for dev in devices:
                            if dev.mac.address == nic.mac.address:
                                if dev.ips not in (None, []):
                                    for ip in dev.ips:
                                        if ip.version == IpVersion.V4:
                                            iface_id = services.get_iface_by_hash(
                                                session, _nic_hash
                                            ).id
                                            ipaddr = IPAddress(
                                                ip_address=ip.address, iface_id=iface_id
                                            )
                                            services.add_or_update_ip_address(
                                                session, ipaddr
                                            )

                # Mark non-existent VMs as deleted
                actual_vms_hashes = [x.vm_hash for x in actual_vms]
                session.query(VM).filter(
                    (VM.engine == current_engine)
                    & (VM.vm_hash.not_in(actual_vms_hashes) & (VM.deleted_at.is_(None)))
                ).update({VM.deleted_at: datetime.now()})
                session.commit()
                logging.info(f"Update vms data from {conn._engine_url}. Done!")
            except Exception as error:
                logging.error(error)

    def sync_vms(self) -> None:
        logging.info("Update vms data...")
        self.last_update = datetime.now()
        with concurrent.futures.ThreadPoolExecutor(
            max_workers=len(self._connections)
        ) as executor:
            executor.map(self.update_vms_data, self._connections)

    def write_history(self):

        with db.get_session() as session:
            actual_vms = session.query(VM).all()

            # Write sync history
            sync_obj = SyncHistory(sync_datetime=self.last_update)
            sync_obj.set_vms(actual_vms)
            services.write_sync_history(session, sync_obj)

    def sync(self, force: bool = False):
        try:
            if not self.is_sync_available and not force:
                return

            self.sync_engines()
            self.sync_vms()
            self.write_history()
        except Exception as error:
            logging.critical(f"Can't sync data: {error}")
            return

    def shutdown(self) -> None:
        self.sync_thread.join()
        self.scheduler.stop()
        self.scheduler.join()

    def find_storage_domain(self, name: str) -> Optional[dict]:
        storage_domains = []

        def process_engine(conn):
            domains = conn.system_service().storage_domains_service().list()

            return {
                "engine": {"name": urlparse(conn.url).netloc, "url": conn._engine_url},
                "domains": domains,
            }

        with concurrent.futures.ThreadPoolExecutor() as executor:
            connections = [conn for conn in self._connections if conn.test()]
            futures = [executor.submit(process_engine, conn) for conn in connections]

            for future in concurrent.futures.as_completed(futures):
                result = future.result()
                if result:
                    storage_domains.append(result)

        engines_with_domain = list(
            filter(
                lambda e: any(
                    [
                        d.name == name
                        and (d.status == StorageDomainStatus.ACTIVE or d.status is None)
                        for d in e["domains"]
                    ]
                ),
                storage_domains,
            )
        )

        return engines_with_domain

    def connect(self):
        for engine in self.engines:
            engine_url = engine.get("url")
            username = engine.get("bot_ipa_username")
            password = engine.get("bot_ipa_password")

            logging.debug(f"Connect to {engine_url}")
            connection = ovirt.Connection(
                url=os.path.join(engine_url, "ovirt-engine", "api"),
                username=username if username else self._config.get("bot_ipa_username"),
                password=password if password else self._config.get("bot_ipa_password"),
                insecure=True,
                timeout=REQUESTS_TIMEOUT,
            )

            # Create engine_url field
            connection._engine_url = engine_url

            try:
                success = connection.test()
                if not success:
                    logging.warning(f"Can't connect to {engine_url}")

            except ovirt.ConnectionError as error:
                logging.error(f"ovirt.ConnectionError: {error}. Skip {engine_url}")
            finally:
                self._connections.append(connection)

    @staticmethod
    def get_engines_status() -> dict:
        with db.get_session() as session:
            data = {"total": 0, "engines": []}
            engines = services.get_engines(session)

            for eng in engines:
                vms_total = (
                    session.query(VM)
                    .filter((VM.engine_id == eng.id) & (VM.deleted_at.is_(None)))
                    .count()
                )
                vms_up = (
                    session.query(VM)
                    .filter(
                        (VM.engine_id == eng.id)
                        & (VM.vm_status == "up")
                        & (VM.deleted_at.is_(None))
                    )
                    .count()
                )
                vms_down = (
                    session.query(VM.vm_status)
                    .filter(
                        (VM.engine_id == eng.id)
                        & (VM.vm_status == "down")
                        & (VM.deleted_at.is_(None))
                    )
                    .count()
                )

                data["engines"].append((eng, vms_total, vms_up, vms_down))
                data["total"] += vms_total

            # Sort engines
            data["engines"] = sorted(data["engines"], key=lambda x: x[0].name)
            return data

    @staticmethod
    def search_vms_by_name(vm_name: str) -> Optional[dict]:
        with db.get_session() as session:
            data = {"count": 0, "vms": []}
            result = services.get_vms_by_name(session, vm_name)

            if not result:
                return None

            data["count"] = len(result)
            for vm in result:
                data["vms"].append((vm, vm.engine))
            return data

    @staticmethod
    def search_vms_by_mac_address(mac_address: str) -> Optional[dict]:
        with db.get_session() as session:
            data = {"count": 0, "vms": []}
            result = services.get_vms_by_mac_address(session, mac_address)

            if not result:
                return None

            data["count"] = len(result)
            for vm in result:
                data["vms"].append((vm, vm.engine))
            return data

    @staticmethod
    def search_vms_by_ip_addresses(addresses: list[str]) -> Optional[dict]:
        with db.get_session() as session:
            data = {"count": 0, "vms": []}
            result = services.get_vms_by_ip_addresses(session, addresses)

            if not result:
                return None

            data["count"] = len(result)
            for vm in result:
                vm_obj = vm
                vm_engine_obj = vm.engine
                ifaces = vm.vm_ifaces
                addresses = [
                    addr
                    for iface in [iface.ip_address for iface in ifaces]
                    for addr in iface
                ]

                data["vms"].append((vm_obj, vm_engine_obj, addresses))
            return data

    @staticmethod
    def search_vms_by_ip_address(ip_address: str) -> Optional[dict]:
        with db.get_session() as session:
            data = {"count": 0, "vms": []}
            result = services.get_vms_by_ip_address(session, ip_address)

            if not result:
                return None

            data["count"] = len(result)
            for vm in result:
                data["vms"].append((vm, vm.engine))
            return data

    @staticmethod
    def get_last_created_vms(count: int) -> Optional[dict]:
        with db.get_session() as session:
            data = {"count": 0, "vms": []}
            result = services.get_last_created_vms(session, count)

            if not result:
                return None

            data["count"] = len(result)

            for vm in result:
                data["vms"].append((vm, vm.engine))

        return data

    @staticmethod
    def get_lost_vms(count) -> Optional[dict]:
        with db.get_session() as session:
            data = {"count": 0, "vms": []}
            result = services.get_lost_vms(session, count)

            if not result:
                return None

            data["count"] = len(result)

            for vm in result:
                data["vms"].append((vm, vm.engine))

        return data

    @staticmethod
    def get_vm_info(vm_name: str) -> Optional[dict]:

        with db.get_session() as session:
            data = {"count": 0, "vms": []}
            result = services.get_vms_by_name(session, vm_name)

            if not result:
                return None

            data["count"] = len(result)

            for vm in result:
                vm_obj = vm
                vm_engine_obj = vm.engine
                ifaces = vm.vm_ifaces
                addresses = [
                    addr
                    for iface in [iface.ip_address for iface in ifaces]
                    for addr in iface
                ]

                data["vms"].append((vm_obj, vm_engine_obj, ifaces, addresses))

            return data

    @staticmethod
    def show_sync_history(count: int = 15) -> Optional[dict]:
        with db.get_session() as session:
            data = {"count": 0, "dates": []}
            dates = services.show_sync_history(session, count)
            data["count"] = len(dates)
            data["dates"] = dates
            return data

    @staticmethod
    def get_sync_diff(datetime: datetime) -> Optional[dict]:
        with db.get_session() as session:
            data = {"count": 0, "vms": [], "sync_date": datetime}
            result = services.get_vms_diff_from_history(session, datetime)
            if not result:
                return

            data["count"] = len(result)
            for v1, v2 in result:
                data["vms"].append((v1, v1.engine, v2))
            return data

    @staticmethod
    def clean_database():
        logging.info(f"Start database cleaning...")
        with db.get_session() as session:
            deleted_vms = services.clear_deleted_vms(session)
            logging.info(f"Deleted {len(deleted_vms)} vms.")

    @property
    def next_update(self):
        return self.last_update + timedelta(seconds=self._config.get("sync_timeout"))

    @property
    def is_sync_available(self):
        return bool(datetime.now() > self.next_update)

    @property
    def last_update(self):
        return self._last_update

    @last_update.setter
    def last_update(self, val):
        self._last_update = val
