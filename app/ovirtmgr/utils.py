import hashlib


def get_vm_hash(vm_id: str, timestemp: float) -> str:
    return hashlib.md5(f"{vm_id}+t{timestemp}".encode()).hexdigest()


def get_nic_hash(vm_hash: str, nic_id: str) -> str:
    return hashlib.md5(f"{vm_hash}+i{nic_id}".encode()).hexdigest()
