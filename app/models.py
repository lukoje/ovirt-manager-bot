from datetime import datetime
from typing import List

from sqlalchemy import ForeignKey, Integer, String, Boolean, DateTime, PickleType
from sqlalchemy.orm import Mapped, mapped_column, relationship

from app.database import Base

import pickle


class Engine(Base):
    __tablename__ = "engines"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    name: Mapped[str] = mapped_column(String, unique=True, index=True, nullable=False)
    url: Mapped[str] = mapped_column(String, nullable=False)
    vm_count: Mapped[int] = mapped_column(Integer, default=False, nullable=True)
    vms: Mapped[List["VM"]] = relationship(
        back_populates="engine", cascade="all, delete-orphan"
    )

    available: Mapped[bool] = mapped_column(Boolean, nullable=False)


class VM(Base):
    __tablename__ = "vms"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    vm_hash: Mapped[str] = mapped_column(
        String, unique=True, index=True, nullable=False
    )
    vm_name: Mapped[str] = mapped_column(String, nullable=False, index=True)
    vm_ifaces: Mapped[List["Interface"]] = relationship(
        "Interface", back_populates="vm", cascade="all, delete-orphan"
    )
    engine_id: Mapped[int] = mapped_column(ForeignKey("engines.id", ondelete="CASCADE"))
    engine: Mapped["Engine"] = relationship("Engine", back_populates="vms")
    vm_status: Mapped[str] = mapped_column(String, nullable=True)
    created_at: Mapped[datetime] = mapped_column(DateTime, default=False)
    deleted_at: Mapped[datetime] = mapped_column(DateTime, nullable=True)
    start_time: Mapped[datetime] = mapped_column(DateTime, nullable=True)
    stop_time: Mapped[datetime] = mapped_column(DateTime, nullable=True)


class Interface(Base):
    __tablename__ = "interfaces"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    iface_hash: Mapped[str] = mapped_column(
        String, unique=True, index=True, nullable=False
    )
    iface_name: Mapped[str] = mapped_column(String, nullable=True, index=True)
    vm_id: Mapped[int] = mapped_column(ForeignKey("vms.id", ondelete="CASCADE"))
    vm: Mapped["VM"] = relationship("VM", back_populates="vm_ifaces")
    mac_address: Mapped[str] = mapped_column(String, nullable=False, index=True)
    ip_address: Mapped[List["IPAddress"]] = relationship(
        "IPAddress", back_populates="iface", cascade="all, delete-orphan"
    )


class IPAddress(Base):
    __tablename__ = "ip_addresses"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    ip_address: Mapped[str] = mapped_column(
        String, nullable=False, index=True, unique=False
    )
    iface_id: Mapped[int] = mapped_column(
        ForeignKey("interfaces.id", ondelete="CASCADE")
    )
    iface: Mapped["Interface"] = relationship("Interface", back_populates="ip_address")


class SyncHistory(Base):
    __tablename__ = "sync_history"

    id: Mapped[int] = mapped_column(Integer, primary_key=True)
    vms: Mapped[bytes] = mapped_column(PickleType, nullable=False)
    sync_datetime: Mapped[datetime] = mapped_column(
        DateTime, nullable=False, index=True
    )

    def set_vms(self, vms: list[VM]):
        self.vms = pickle.dumps(vms)

    def get_vms(self):
        return pickle.loads(self.vms)
