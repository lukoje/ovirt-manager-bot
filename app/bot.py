from telegram.ext import Application, CommandHandler, filters

from app.ovirtmgr import OvirtManager
import logging


class OvirtBot(object):
    app: Application = None
    om: OvirtManager

    @classmethod
    def init(cls, token: str) -> None:
        cls.app = Application.builder().token(token).build()

    @classmethod
    def set_ovirt_manager(cls, manager: OvirtManager) -> None:
        cls.om = manager

    @classmethod
    def register_handlers(cls, commands: dict, bot_access: list):
        for command_name, command_handler in commands.items():
            if not command_name == "start":
                cls.app.add_handler(
                    CommandHandler(
                        command_name,
                        command_handler,
                        filters=filters.User(user_id=bot_access)
                        | filters.Chat(chat_id=bot_access),
                    )
                )
            else:
                cls.app.add_handler(CommandHandler(command_name, command_handler))

    @classmethod
    def exit_gracefuly(cls):
        logging.info("Exit gracefuly...")
        cls.app.shutdown()
        cls.om.shutdown()
