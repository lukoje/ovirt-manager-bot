from telegram import Update
from telegram.ext import ContextTypes
from telegram.constants import ParseMode

from app.ovirtmgr import OvirtManager
from app.templates import render_template
from app.utils import parse_datetime, portionise_reply

import openpyxl
from openpyxl.styles import Border, Side


def create_excel_document(vms, date) -> str:

    wb = openpyxl.Workbook()
    sheet = wb.active
    headers = [
        "Name",
        "Engine",
        f"Status on {date.strftime('%d.%m.%Y  %H:%M:%S')}",
        "",
        "Current Status",
        "Engine URL",
    ]

    for col_num, header in enumerate(headers, 1):
        sheet.cell(row=1, column=col_num, value=header)

    data = []
    for v1, engine, v2 in vms["vms"]:
        data.append(
            [v1.vm_name, engine.name, v2.vm_status, "→", v1.vm_status, engine.url]
        )

    for row_num, row_data in enumerate(data, 2):
        for col_num, value in enumerate(row_data, 1):
            sheet.cell(row=row_num, column=col_num, value=value)

    for rows in sheet.iter_rows(
        min_row=1, max_row=sheet.max_row, min_col=1, max_col=sheet.max_column
    ):
        for cell in rows:
            cell.border = Border(
                left=Side(border_style="thin"),
                right=Side(border_style="thin"),
                top=Side(border_style="thin"),
                bottom=Side(border_style="thin"),
            )

    for column in sheet.columns:
        max_length = 0
        column = [cell for cell in column]
        for cell in column:
            try:
                if len(str(cell.value)) > max_length:
                    max_length = len(cell.value)
            except:
                pass
        adjusted_width = max_length + 2
        sheet.column_dimensions[column[0].column_letter].width = adjusted_width

    file_path = f"diff_table.xlsx"
    wb.save(file_path)
    return file_path


async def vm_sync_diff(update: Update, context: ContextTypes.DEFAULT_TYPE):
    arguments = context.args
    if not arguments:
        data = OvirtManager.show_sync_history()
        if not data:
            await update.message.reply_text(f"Изменений не найдено.")
            return

        msg = render_template("vm_sync_history.j2", data)
        await portionise_reply(update.message, text=msg, parse_mode=ParseMode.HTML)
    else:
        date = " ".join(arguments)
        date = parse_datetime(date)

        if date is None:
            await update.message.reply_text(
                f"Неверный формат даты. Доступные форматы:\n - dd.mm.yyyy HH:MM:SS\n - yyyy.mm.dd HH:MM:SS\n - dd.mm.yyyy"
            )
            return

        try:
            data = OvirtManager.get_sync_diff(date)

            if not data:
                await update.message.reply_text(f"Изменений не найдено.")
                return

            msg = render_template("vm_sync_diff.j2", data)
            file_path = create_excel_document(data, date)

            await portionise_reply(update.message, text=msg, parse_mode=ParseMode.HTML)

            with open(file_path, "rb") as document:
                await update.message.reply_document(document)
        except Exception as e:
            await update.message.reply(f"Произошла ошибка: {e}")
