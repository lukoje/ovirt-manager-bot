from telegram import Update
from telegram.ext import ContextTypes
from telegram.constants import ParseMode

from app.templates import render_template


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    data = {"chat_id": update.message.chat.id, "user_id": update.message.from_user.id}
    msg = render_template("start.j2", data)

    await update.message.reply_text(msg, parse_mode=ParseMode.HTML)
