from telegram import Update
from telegram.ext import ContextTypes
from telegram.constants import ParseMode

from app.ovirtmgr import OvirtManager
from app.templates import render_template
from app.utils import portionise_reply


async def vm_last(update: Update, context: ContextTypes.DEFAULT_TYPE):
    arguments = context.args
    count = 10
    if arguments:
        count = int(arguments[0].strip())

    data = OvirtManager.get_last_created_vms(count)

    if data is None:
        await update.message.reply(f"Ничего нету...")
        return

    msg = render_template("last_vms.j2", data)

    await portionise_reply(update.message, text=msg, parse_mode=ParseMode.HTML)
