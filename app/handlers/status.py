from telegram import Update
from telegram.ext import ContextTypes
from telegram.constants import ParseMode

from app.bot import OvirtBot
from app.config import DATETIME_FORMAT
from app.ovirtmgr import OvirtManager
from app.templates import render_template


async def status(update: Update, context: ContextTypes.DEFAULT_TYPE):
    data: dict = OvirtManager.get_engines_status()
    data["last_update"] = OvirtBot.om.last_update.strftime(DATETIME_FORMAT)

    msg = render_template("engine_status.j2", data)
    await update.message.reply_text(msg, parse_mode=ParseMode.HTML)
