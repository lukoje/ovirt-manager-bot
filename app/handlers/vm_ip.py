from telegram import Update
from telegram.ext import ContextTypes
from telegram.constants import ParseMode

from app.ovirtmgr import OvirtManager
from app.templates import render_template
from app.utils import portionise_reply, parse_ip_address


async def vm_ip(update: Update, context: ContextTypes.DEFAULT_TYPE):
    arguments = context.args
    if not arguments:
        await update.message.reply_text(f"Usage: /vm_ip <ip-adderess>")
        return

    query = arguments[0].strip()
    ip_address = parse_ip_address(query)

    if ip_address is not None:
        data = OvirtManager.search_vms_by_ip_address(ip_address)

        if data is None:
            await update.message.reply_text(f"ВМ с таким ip-адресом не найдена :(")
            return

        data["query"] = ip_address
        msg = render_template("search_vms.j2", data)
    else:
        data = OvirtManager.get_vm_info(query)
        if data is None:
            await update.message.reply_text(f"ВМ с таким именем не найдена :(")
            return

        data["query_type"] = "ip"
        msg = render_template("vm_info.j2", data)

    await portionise_reply(update.message, text=msg, parse_mode=ParseMode.HTML)
