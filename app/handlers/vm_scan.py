import ipaddress

from telegram import Update
from telegram.ext import ContextTypes
from telegram.constants import ParseMode

from app.ovirtmgr import OvirtManager
from app.templates import render_template
from app.utils import portionise_reply


async def vm_scan(update: Update, context: ContextTypes.DEFAULT_TYPE):
    arguments = context.args
    if not arguments:
        await update.message.reply_text(f"Usage: /vm_scan <network or ip-range>")
        return

    query = arguments[0].strip()
    addresses = []
    try:
        if len(query.split("-")) == 2:
            start_ip = ipaddress.IPv4Address(query.split("-")[0].strip())
            end_ip = ipaddress.IPv4Address(query.split("-")[1].strip())
            addresses = [
                str(ipaddress.ip_address(int(ip)))
                for ip in range(int(start_ip), int(end_ip) + 1)
            ]
        elif len(query.split("-")) < 2:
            network = ipaddress.ip_network(query.strip())
            addresses = [str(ip) for ip in network]
        else:
            await update.message.reply_text(
                f"Example: /vm_scan <192.168.0.0/24 or 192.168.0.100-192.168.0.200>"
            )
            return
    except ipaddress.AddressValueError as error:
        await update.message.reply_text(f"AddressValueError: {error}!")
        return
    except ValueError as error:
        await update.message.reply_text(f"ValueError: {error}!")
        return

    data = OvirtManager.search_vms_by_ip_addresses(addresses)

    if data is None or data["count"] == 0:
        await update.message.reply_text(f"ВМ в данной сети не найдены.")
        return

    data["query"] = query
    msg = render_template("vm_scan.j2", data)

    await portionise_reply(update.message, text=msg, parse_mode=ParseMode.HTML)
