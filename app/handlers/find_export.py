from telegram import Update
from telegram.ext import ContextTypes
from telegram.constants import ParseMode

from app.templates import render_template
from app.bot import OvirtBot


async def find_export(update: Update, context: ContextTypes.DEFAULT_TYPE):
    arguments = context.args
    if not arguments:
        await update.message.reply_text(f"Usage: /find_export <Storage Domain Name>")
        return

    name = arguments[0].strip()
    data = OvirtBot.om.find_storage_domain(name)

    msg = render_template("find_export.j2", data={"export_name": name, "data": data})

    await update.message.reply_text(msg, parse_mode=ParseMode.HTML)
