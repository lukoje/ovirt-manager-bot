from telegram import Update
from telegram.ext import ContextTypes
from telegram.constants import ParseMode

from app.ovirtmgr import OvirtManager
from app.templates import render_template
from app.utils import parse_mac_address, portionise_reply


async def vm_mac(update: Update, context: ContextTypes.DEFAULT_TYPE):
    arguments = context.args
    if not arguments:
        await update.message.reply_text(f"Usage: /vm_mac <mac-adderess>")
        return

    query = arguments[0].strip()
    mac_address = parse_mac_address(query)

    if mac_address is not None:

        data = OvirtManager.search_vms_by_mac_address(mac_address)
        if data is None:
            await update.message.reply_text(f"ВМ с таким мак-адресом не найдена :(")
            return

        data["query"] = mac_address
        msg = render_template("search_vms.j2", data)

    else:
        data = OvirtManager.get_vm_info(query)

        if data is None:
            await update.message.reply_text(f"ВМ с таким именем не найдена :(")
            return

        data["query_type"] = "mac"
        msg = render_template("vm_info.j2", data)

    await portionise_reply(update.message, text=msg, parse_mode=ParseMode.HTML)
