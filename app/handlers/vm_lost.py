from telegram import Update
from telegram.ext import ContextTypes
from telegram.constants import ParseMode

from app.ovirtmgr import OvirtManager
from app.templates import render_template
from app.utils import portionise_reply

import openpyxl
from openpyxl.styles import Border, Side


def create_excel_document(vms) -> str:

    wb = openpyxl.Workbook()
    sheet = wb.active
    headers = [
        "Name",
        "Engine",
        f"Stop time",
        "Current Status",
        "Delete on",
        "Engine URL",
    ]

    for col_num, header in enumerate(headers, 1):
        sheet.cell(row=1, column=col_num, value=header)

    data = []
    for vm, engine in vms["vms"]:
        data.append(
            [
                vm.vm_name,
                engine.name,
                vm.stop_time.strftime("%d.%m.%Y %H:%M:%S"),
                vm.vm_status,
                vm.deleted_at.strftime("%d.%m.%Y %H:%M:%S") if vm.deleted_at else "-",
                engine.url,
            ]
        )

    for row_num, row_data in enumerate(data, 2):
        for col_num, value in enumerate(row_data, 1):
            sheet.cell(row=row_num, column=col_num, value=value)

    for rows in sheet.iter_rows(
        min_row=1, max_row=sheet.max_row, min_col=1, max_col=sheet.max_column
    ):
        for cell in rows:
            cell.border = Border(
                left=Side(border_style="thin"),
                right=Side(border_style="thin"),
                top=Side(border_style="thin"),
                bottom=Side(border_style="thin"),
            )

    for column in sheet.columns:
        max_length = 0
        column = [cell for cell in column]
        for cell in column:
            try:
                if len(str(cell.value)) > max_length:
                    max_length = len(cell.value)
            except:
                pass
        adjusted_width = max_length + 2
        sheet.column_dimensions[column[0].column_letter].width = adjusted_width

    file_path = f"lost_vms.xlsx"
    wb.save(file_path)
    return file_path


async def vm_lost(update: Update, context: ContextTypes.DEFAULT_TYPE):
    arguments = context.args
    count = 10
    if arguments:
        if arguments[0].strip().isnumeric():
            count = int(arguments[0].strip())
        elif arguments[0].strip().isalpha() and arguments[0].strip() == "all":
            count = 0

    data = OvirtManager.get_lost_vms(count)

    if data is None:
        await update.message.reply(f"Ничего нету...")
        return

    if count <= 0:
        file_path = create_excel_document(data)

        with open(file_path, "rb") as document:
            await update.message.reply_document(document)
            return
    else:
        msg = render_template("lost_vms.j2", data)

    await portionise_reply(update.message, text=msg, parse_mode=ParseMode.HTML)
