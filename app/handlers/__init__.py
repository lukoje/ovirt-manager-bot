from .start import start
from .status import status
from .vm_ip import vm_ip
from .vm_scan import vm_scan
from .vm_last import vm_last
from .vm_lost import vm_lost
from .vm_mac import vm_mac
from .vm_name import vm_name
from .vm_sync_diff import vm_sync_diff
from .find_export import find_export

__all__ = ["COMMAND_HANDLERS"]

COMMAND_HANDLERS = {
    "start": start,
    "status": status,
    "vm_name": vm_name,
    "vm_mac": vm_mac,
    "vm_ip": vm_ip,
    "vm_scan": vm_scan,
    "vm_last": vm_last,
    "vm_lost": vm_lost,
    "vm_sync_diff": vm_sync_diff,
    "find_export": find_export,
}
