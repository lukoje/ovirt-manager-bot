from telegram import Update
from telegram.ext import ContextTypes
from telegram.constants import ParseMode

from app.ovirtmgr import OvirtManager
from app.templates import render_template
from app.utils import portionise_reply


async def vm_name(update: Update, context: ContextTypes.DEFAULT_TYPE):
    arguments = context.args
    if not arguments:
        await update.message.reply_text(f"Usage: /vm_name <name>")
        return

    name = arguments[0].strip()
    name_filter = f"%{name}%"

    data = OvirtManager.search_vms_by_name(name_filter)
    if data is None:
        await update.message.reply_text(f"ВМ с таким именем не найдена :(")
        return

    data["query"] = name
    msg = render_template("search_vms.j2", data)

    await portionise_reply(update.message, text=msg, parse_mode=ParseMode.HTML)
