import logging
import sys


def setup_logging(logfile_path: str = None, verbose: bool = False):

    handlers = [logging.StreamHandler(sys.stdout)]
    if logfile_path is not None:
        handlers += [logging.FileHandler(logfile_path)]

    logging.basicConfig(
        level=logging.DEBUG if verbose else logging.INFO,
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        handlers=handlers,
        force=True,
    )
    logging.getLogger("sqlalchemy").setLevel(logging.ERROR)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.getLogger("httpx").setLevel(logging.WARNING)


