import re

from datetime import datetime

VM_ENGINE_PROFILE = (
    "https://{engine_name}/ovirt-engine/webadmin/?locale=en_US#vms-general;name={name}"
)

MAC_ADDRESS_MATCH = r"^([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})|([0-9a-fA-F]{4}[-][0-9a-fA-F]{4}[-][0-9a-fA-F]{4})$"

DATE_FORMAT = "%d.%m.%Y"
DATETIME_FORMAT: str = "%d.%m.%Y %H:%M%S"


def sqlite_dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


def portionise_text(text, limit=4096):
    lines = text.split("\n")
    blocks = []
    current_block = []
    current_length = 0

    for line in lines:
        if current_length + len(line) <= limit:
            current_block.append(line)
            current_length += len(line)
        else:
            blocks.append("\n".join(current_block))
            current_block = [line]
            current_length = len(line)

    if current_block:
        blocks.append("\n".join(current_block))
    return blocks


async def portionise_reply(message, text, *args, **kwargs) -> None:
    text_arr = portionise_text(text)
    for text in text_arr:
        await message.reply_text(text=text, *args, **kwargs)


def parse_ip_address(ip_address: str):
    if re.match(r"[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}", ip_address):
        return ip_address
    return None


def parse_mac_address(mac_address: str):
    if re.match(r"([0-9A-Fa-f]{2}[:-]){5}([0-9A-Fa-f]{2})", mac_address):
        return re.sub(r"-", ":", mac_address).lower()
    elif re.match(r"[0-9A-Fa-f]{12}", mac_address) or re.match(
        r"([0-9a-fA-F]{4}[-][0-9a-fA-F]{4}[-][0-9a-fA-F]{4})", mac_address
    ):
        t = re.sub(r"[:-]", "", mac_address).lower()
        return ":".join([t[i : i + 2] for i in range(0, len(t), 2)])
    else:
        return None


def parse_datetime(date_string: str) -> datetime:
    date_string = date_string.strip()
    date_patterns = [
        (
            r"\d{2}\.\d{2}.\d{4}\s*\d{2}:\d{2}:\d{2}",
            "%d.%m.%Y %H:%M:%S",
        ),  # dd.mm.yyyy HH:MM:SS
        (
            r"\d{4}\.\d{2}.\d{2}\s*\d{2}:\d{2}:\d{2}",
            "%Y.%m.%d %H:%M:%S",
        ),  # yyyy.mm.dd HH:MM:SS
        (r"\d{2}\.\d{2}.\d{4}", "%d.%m.%Y"),  # dd.mm.yyyy
    ]

    format = None
    for p, f in date_patterns:
        if re.match(p, date_string):
            format = f
            break

    if format is None:
        return

    combined_datetime = datetime.strptime(date_string, format)

    return combined_datetime
