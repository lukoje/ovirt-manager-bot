import threading
import time

import logging


class Timer(object):
    _start = None
    _timeout = None
    _function = None
    _args = None

    def __init__(self, timeout, function, *args, **kwargs):
        self._function = function
        self._timeout = timeout
        self._start = time.time()
        self._args = args
        self._kwargs = kwargs

    # Resets the time of last reset
    def reset(self):
        self._start = time.time()

    # Calls the function if enough time has elapsed
    def check(self):
        if time.time() - self._start >= self._timeout:
            self._function(*self._args, **self._kwargs)
            self.reset()

    # Sets the timeout
    def set_timeout(self, timeout):
        self._timeout = timeout


class Scheduler(object):
    _timers = []
    _interval = None

    def __init__(self, timers, interval=1):
        super(Scheduler, self).__init__()
        self._is_running = False

        self._interval = interval
        if not timers or len(timers) == 0:
            return
        for timer in timers:
            if not isinstance(timer, Timer):
                logging.error("Scheduler: object is not Timer: %s" % timer)
                continue
            timer.reset()
            self._timers.append(timer)

    # Adds another timer
    def add_timer(self, timer):
        self._timers.append(timer)

    def stop(self):
        self._is_running = False

    def restart(self):
        self._is_running = True

    # Runs the Timer checking loop
    def run(self):
        self._is_running = True
        try:
            while self._is_running:
                for timer in self._timers:
                    timer.check()
                time.sleep(self._interval)
        except KeyboardInterrupt:
            return

        except Exception as error:
            logging.error(f"Scheduler Error: {error}")


class ThreadedScheduler(Scheduler, threading.Thread):
    def __init__(self, timers, interval=1):
        super().__init__(timers, interval)
