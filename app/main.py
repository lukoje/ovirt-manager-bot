from telegram import Update


from app.database import db
from app.bot import OvirtBot
from app.config import load_config
from app.args import setup_arguments
from app.handlers import COMMAND_HANDLERS
from app.ovirtmgr import OvirtManager
from app.log import setup_logging
import logging


def main():
    args = setup_arguments()
    config = load_config(args.config)
    setup_logging(args.logfile or config.get("log_file"), args.verbose)

    logging.info("Start oVirt Manager Bot")

    db.init(config)

    OvirtBot.init(token=config.get("bot_token"))
    OvirtBot.register_handlers(COMMAND_HANDLERS, config.get("bot_access"))

    om = OvirtManager(config)

    OvirtBot.set_ovirt_manager(om)
    OvirtBot.om.connect()

    if config.get("sync_on_startup"):
        OvirtBot.om.sync(force=True)

    OvirtBot.app.run_polling(
        allowed_updates=Update.ALL_TYPES, drop_pending_updates=True
    )

    OvirtBot.exit_gracefuly()
