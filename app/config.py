import json
import logging
import os
import sys


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
TEMPLATES_DIR = os.path.join(BASE_DIR, "templates")
DATETIME_FORMAT = "%d.%m.%Y %H:%M"
REQUESTS_TIMEOUT = 30


def load_config(config_path):
    if not os.path.exists(config_path):
        logging.error(f"Config file not found: {config_path}")
        sys.exit(1)
    try:
        with open(config_path, "r") as file:
            config = json.load(file)
        logging.info("Config loaded successfully")
        return config
    except json.JSONDecodeError as e:
        logging.error(f"Error decoding JSON from the config file: {e}")
        sys.exit(1)
    except Exception as e:
        logging.error(f"Unexpected error loading config file: {e}")
        sys.exit(1)
