FROM python:3.11-alpine

COPY . /app
WORKDIR /app

RUN apk --update add --no-cache \
	build-base \
	curl \
	curl-dev \
	openssl \
	openssl-dev \
	libxml2 \
	libxml2-dev && \
	pip install --upgrade pip && \
	pip install -r requirements.txt && \
	pip cache purge && \
	mkdir -p /var/lib/ovirt-manager-bot && \
	apk del build-base \
	curl-dev \
	openssl-dev \
	libxml2-dev

CMD [ "python", "-m", "app", "-c", "config.json" ]